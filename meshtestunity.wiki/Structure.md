# Structure of the program

## Main structure

### Overview

The whole project is grouped into different objects which can be seen in the following figure

![UnityOverview.drawio.png](uploads/cc4e40f01d141dbdc5903b61cc2e5216/UnityOverview.drawio.png)

#### Parent Mesh

The Parent Mesh includes a "Collision Canvas" this is used to trigger collision events with the laser pointer to interact with mesh for drawing.

The Main Mesh includes the important scripts for rendering and coloring the mesh. This includes:

* **Mesh Filter:** to store the loaded mesh
* **Mesh Generator**: Starts and handles the loading of the mesh via the File Parsers
* **Mesh Renderer**: Unity Asset to render the Mesh
* **Mesh color**: Handles the look of the mesh. ncluding parsing and animation of transmembrane voltages, drawing and display of the fibrotic tissue
* **Draw on mesh handler:** Encapsulates the drawing on the mesh including loading/saving drawing files and handle collision calls if the laser point collides with the mesh and drawing is enabled
* **Mesh collider:** Used to help with collision detection
* **Mesh material:** Custom Material to enable pointwise colors. Is further defined in the "Vertex Color"-Shader

#### VR-Player

![PlayerObjects.png](uploads/cade38cd7087bb9f349372a22bd10263/PlayerObjects.png){width="267" height="281"}

The Player object consists of many SteamVR2 prebuild objects which can be looked at at their documentation.

Changes were made to the right hand, which contains the laser pointer ("Pointer") as well as the input module used to interact with buttons and object

The VRCamera which is the min camera includes the logos which are based on their placement locked to the camera screen

#### Color Scale

The Color Scale object includes the 3 color blocks and their label with are handled in the "MeshColor" script

#### Floor and lighting

This objects consists of the floor plane and two lights to illuminate the scene.

#### Menu

The menu is split into three parts:

* The "Data selection" window is used to select the folder of the mesh, fibrosis and transmembrane voltage data. Also it inlcudes a button for starting the loading process and for closing the whole application
* The SimpleFileBrowser is used to browes through the files on the pc. Further details can be found herer https://github.com/yasirkula/UnitySimpleFileBrowser
* The "DrawingCanvas" is used to modify the drawing on the mesh. Here the erraser mode can be turned on and off. Also all drawings can be deleted. In addition the storage folder of the drawing files can be selected and the current drawing can be stored or a drawing from the folder can be loaded

#### Debug Scripts

Here Debug scripts could be attatched. E.g. to start the loading process directly without the need of button interactions.

### Parsing files

Every class for each file which is parsed inherits the IFileParser Interface. This leads to the following structure which, also shows what kind of files are used:

![ClassDiagramParsers.drawio.png](uploads/60b6a06ee4d1cf520ea9c09268398562/ClassDiagramParsers.drawio.png){width="543" height="272"}

### Messaging

All Messages displayed in the consol or on the "Data-Selection" panel are handled via the "StatusManager" script which follows a singletone pattern to avoid unnessecary calls