# Defined features

**/F000/** Menu for parsing in different files

**/F001/** Read in of mesh data in form of .pts and .elem files

**/F010/** Parsing of .igb files to view time series of pointwise data (mainly action potentials)

**/F015/** Fluent display of this time series in the program to see reentry tachycardias

**/F017/** Display of a custom coloring method for the different action potentials including a color bar which shows the minimal, middle and maximal potential

**/F020/** Parsing of .regele files to display severe and mild fibrotic tissue on the loaded model

**/F025/** Innovative display of these fibrotic tissues with the help of a custom graphic shader to enable seeing fibrotic tissue and the action potential animation together

**/F030/** Intuitive marking of important regions with the help of a VR joystick 

**/F040/** Correction of these markings with the help of an erase and reset tool

**/F050/** Usage of VR technology to achieve a more intuitive and easy to learn program

**/F051/** Head movement and rotation tracking to view the model from any direction

**/F052/** Intuitive rotation of the model form /F001/ with the VR controllers to see it from any angle possible

**/F055/** Movement in the scene with the VR controllers for confined spaces

**/F056/** Movement in the scene through walking around the model with the VR headset if enough space is available