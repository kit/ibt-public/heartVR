# Heart VR

This program is meant to render heart models from openCARP and annotate ablation lines on the model.

In the following, you can find out more about this software:

[Basic Structure](structure)

[Features](features)

[Start up](startUp)

[Common problems](problems)

If you have any questions or issues regarding this software message me at Mattermost, Pascal.Maierhofer@student.kit.edu or Pascal.Maierhofer@gmx.de 