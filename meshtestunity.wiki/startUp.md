# Start up

Here you find everything to install and start the program

## Software and Hardware Requirements

### Hardware

* The program is tested and developed for the HTC VIVE, however other steamVR2 compatible headsets should work as well
* PC with minum one HDMI out and satisfies the recommended specs from https://store.steampowered.com/app/250820/SteamVR/

### Software Starting

* Steam and Steam VR installed on the PC
* Drivers for the HTC VIVE

#### Software Developing

* Unity 3D (2022.3.4f1)
* IDE for editing e.g. Microsoft Visual Studio

## Build Start up

* Start Steam 
* Open the .exe file which you got from the releases