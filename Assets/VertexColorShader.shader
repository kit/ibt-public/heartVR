Shader "Custom/VertexColor" {
	
		Properties{
		_DrawFibrosis("Draw Fibrosis",Range(0,1))=1
		_DrawOnMesh("Draw on Mesh",Range(0,1))=1
		_MarkingColor("Color Tint", Color) = (1,1,1,1)
		}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		//Cull Off
		CGPROGRAM
		#pragma surface surf SimpleLambert vertex:vert
		#pragma target 3.0

		
		struct Input {
			float4 vertColor;
			float2 uv_MainTex;
		};
		float _DrawFibrosis;
		float _DrawOnMesh;
		float4 _MarkingColor;

				// All components are in the range [0�1], including hue.
		inline float3 rgb2hsv(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
			float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

			float d = q.x - min(q.w, q.y);
			float e = 1.0e-10;
			return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		
		// All components are in the range [0�1], including hue.
		inline float3 hsv2rgb(float3 c)
		{
			float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}


		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float hue=rgb2hsv(v.color).x;
			float2 uv2=v.texcoord1.xy;
			float4 newColor=v.color; 
			//Set downscaled saturation for fibrotic tissue
			if(_DrawFibrosis>0.5){
				if(uv2.x>0.75){
					newColor=float4(hsv2rgb(float3(hue,0.6,1)),1);
				} else if(uv2.x>0.4){
				newColor=float4(hsv2rgb(float3(hue,0.7,1)),1);
				}
				
			}

			//Show drawing on mesh
			float2 uv3=v.texcoord2.xy;
			if(uv3.x>0&&_DrawOnMesh){
			newColor=_MarkingColor;
			}
			
			o.vertColor = newColor;
		}


		half4 LightingSimpleLambert(SurfaceOutput s, half3 lightDir, half atten)
		{
			half NdotL = dot(s.Normal, lightDir);
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten);
			c.a = s.Alpha;
			return c;
		}






		void surf(Input IN, inout SurfaceOutput o)
		{


			// Assign the final color to the output
			o.Albedo = IN.vertColor.rgb;
			//o.Alpha = IN.vertexColor.a * _Color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}