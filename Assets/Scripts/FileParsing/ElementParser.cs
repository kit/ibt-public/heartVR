﻿using System.Collections.Generic;
using System.Threading.Tasks;


/// <summary>
/// Used to parse in Elements (e.g. Triangles) from an stored file into an list of conections
/// </summary>
public abstract class ElementParser : IFileParser
{
    public abstract Task Parse(string path);
    protected List<int> readInElements = new();
    protected List<int> readInElementTags = new();
    protected List<string> allowedElements;

    public ElementParser(List<string> allowedElements)
    {
        this.allowedElements = allowedElements;
    }

    public List<int> ReadInElementTags { get { return readInElementTags; } }
    public List<int> ReadInElements   // property
    {
        get { return readInElements; }   // get method
        set { readInElements = value; }  // set method
    }
}

