﻿using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// Parses in Files to be displayed as fibrotic tissue 
/// Stores points where fibrotic tissue is present
/// </summary>
abstract class FibrosisParser : IFileParser
{
    protected List<int> fibrosisElements= new List<int>();

    public List<int> FibrosisElements { get { return fibrosisElements; } }

    public abstract Task Parse(string path);
}

