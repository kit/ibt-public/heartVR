using System.Threading.Tasks;

/// <summary>
/// Interface for parsing files on the hard drive
/// </summary>
public interface IFileParser
{
   
    public Task Parse(string path);
    

}
