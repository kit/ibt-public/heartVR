using System.IO;
using System.Threading.Tasks;


/// <summary>
/// Parses in a pts File to get the stored points
/// Points are stored in readInPoints from parent class
/// </summary>
public class PTSParser : PointParser
{
    public async override Task Parse(string path)
    {
        int counter = 0;//Counts number of points
        StreamReader inp_stream = new(path);
        while (!inp_stream.EndOfStream)
        {
            string line = inp_stream.ReadLine();
            string[] sArray = line.Split(' ');
            if (sArray.Length == 3)
            {
                readInPoints.Add(ArrayToVertex(sArray));


                // Logic for freeing the main thread every 100 points and updating the status label accordingly
                counter++;
                if (counter % 100 == 0)
                {
                    if (counter % 1000 == 0)
                    {
                        StatusManager.Instance.SetStatus(counter.ToString("#,##0") + " Points added");
                    }
                    await Task.Yield();
                }
            }
        }
    }
}