﻿using System.IO;
using System.Threading.Tasks;

/// <summary>
/// Can be used to parse in fibrotic information from vtk files marked with Fib
/// </summary>
class FibrosisVTKParser : FibrosisParser
{
    public override async Task Parse(string path)
    {
        if (!path.EndsWith(".vtk"))
        {
            StatusManager.Instance.SetStatus(path + "is not a .vtk file for fibrosis data");
            return;
        }
        int counter = 0;
        int numberElements = 0;
        bool reachedFibrosis = false;
        StreamReader inp_stream = new(path);

        /*if(int.TryParse(inp_stream.ReadLine(), out numberElements) )
        {

        }*/
        int triangleIndexCounter = 0;
        while (!inp_stream.EndOfStream)
        {
            string inputLine = await inp_stream.ReadLineAsync();
           
            if (reachedFibrosis)
            {
                string[] sArray=inputLine.Split(' ');
                foreach (string entry in sArray)
                {
                    if (int.TryParse(entry, out int intEntry))
                    {
                        if (intEntry == 1)
                        {
                            fibrosisElements.Add(triangleIndexCounter);
                            ++counter;
                        }
                        triangleIndexCounter++;
                        

                    }
                }


                if (counter % 100 == 0)
                {
                    StatusManager.Instance.SetStatus(counter + " from " + numberElements + " Fibrosis Read in");
                    await Task.Yield();
                }
            }
            //Read from fib identifier until Id identifier
            reachedFibrosis = (inputLine.StartsWith("Fib") || reachedFibrosis) && !inputLine.StartsWith("Ids");
        }
    }
}
