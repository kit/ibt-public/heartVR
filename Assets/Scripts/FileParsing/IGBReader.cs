using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;


/// <summary>
/// Can be used to read in .igb binary files and converts them to a float array. 
/// Also calculates the minimum and maximum values of the file
/// Result is stored in animationData
/// </summary>
public class IGBReader : IFileParser
{

    private int numberOfVertices;
    private int numberOfTimestemps;
    private float[,] animationData;
    public float[,] AnimationData { get { return animationData; } }
    public int NumTimestamps//Used to access the number of vertices variable
    {
        get
        {
            return numberOfTimestemps;
        }
        set { numberOfTimestemps = value; }
    }
    public float minValue, maxValue;

    public IGBReader()
    {
        numberOfVertices = 0;
        numberOfTimestemps = 0;
        minValue = float.MaxValue;
        maxValue = float.MinValue;
    }


    /// <summary>
    /// Parses the content part of the igb file from byte array into an float array
    /// </summary>
    /// <param name="content">byte array which includes only the float parameters as bytes</param>
    /// <returns>Converted float array</returns>
    private async Task<float[,]> ParseContent(byte[] content)
    {
        float[,] valueArray = new float[numberOfTimestemps, numberOfVertices];

        for (int i = 0; i < content.Length / 4; i++)
        {
            //Calculate indices
            int vertexIndex = i % numberOfVertices;
            int timeIndex = i / numberOfVertices;
            //Convert byte to float (4byte equals one float)
            float val = System.BitConverter.ToSingle(content, i * 4);

            //Assigns the value to the right array position and updates the min max values
            valueArray[timeIndex, vertexIndex] = val;
            UpdateMinMax(val);

            //Update status if sequence of one vertex is read in also yields to free the main thread for other tasks
            if (vertexIndex == 0)
            {
                StatusManager.Instance.SetStatus(timeIndex + " Timesteps read in");
                await Task.Yield();
            }
        }
        return valueArray;
    }





    /// <summary>
    /// Takes a value and updates the min and max value seen in this parse instance
    /// </summary>
    /// <param name="value">The new value</param>
    private void UpdateMinMax(float value)
    {
        minValue = Mathf.Min(minValue, value);
        maxValue = Mathf.Max(maxValue, value);

    }

    /// <summary>
    /// Scans the header for the selected variable and returns its value
    /// </summary>
    /// <param name="header">Whole header as string</param>
    /// <param name="nameOfVariable">Variable name which should be read in</param>
    /// <returns>the integer value of the variable</returns>
    private int GetNumberFromHeader(string header, string nameOfVariable)
    {
        //Pattern how the variables are stored: "VariableName":"Value"
        string headerPatternX = $@"{nameOfVariable}:([0-9]+)";
        Match match = Regex.Match(header, headerPatternX);

        //Process if variable is found
        if (match.Success)
        {
            //0 is whole string 1 is the corresponding value
            string stringNumber = match.Groups[1].Value;
            //Parse to int and return the value
            if (int.TryParse(stringNumber, out int number))
            {
                return number;
            }
            else
            {
                Debug.Log("Couldn't parse " + stringNumber);
            }
        }
        else
        {
            Debug.Log("Didn't find" + headerPatternX);
        }
        return 0;
    }

    /// <summary>
    /// Parses an igb binary file and writes it contents to the animationData variable
    /// </summary>
    /// <param name="filePath">File path of the igb file</param>
    public async Task Parse(string path)
    {
        animationData = new float[0, 0];

        if (File.Exists(path))
        {
            try
            {
                byte[] cropedArray;
                StatusManager.Instance.SetStatus("Started reading in the animation file");
                byte[] data = await File.ReadAllBytesAsync(path);
                StatusManager.Instance.SetStatus("Finished reading in. Now processing");

                //Divide into header and content
                cropedArray = new byte[data.Length - 1024]; //"First 1024 bytes are the header"
                byte[] header = data.Take(1024).ToArray();
                string s_header = Encoding.ASCII.GetString(header);
                cropedArray = data.Skip(1024).ToArray();

                //Prozess header
                numberOfVertices = GetNumberFromHeader(s_header, "x");
                numberOfTimestemps = GetNumberFromHeader(s_header, "t");
                Debug.Log("Number of vertices is: " + numberOfVertices);
                Debug.Log("Found " + numberOfTimestemps + " timestamps");

                //Convert content of the file
                StatusManager.Instance.SetStatus("Start Converting " + cropedArray.Length / 4 + " floats");
                animationData = await ParseContent(cropedArray);
                StatusManager.Instance.SetStatus("Ended Converting");
            }
            catch (IOException e)
            {
                Debug.LogError("Error reading the binary file" + e.Message);
            }


        }
        else
        {
            Debug.LogError("File " + path + " does not exist");
        }
        Debug.Log("Length of array" + animationData.Length);
    }
}

