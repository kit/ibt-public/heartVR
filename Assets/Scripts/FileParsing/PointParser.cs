﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using UnityEngine;


/// <summary>
/// Pases an file to receive points of an mesh 
/// Points are stored in readInPoints
/// </summary>
public abstract class PointParser : IFileParser
{
    protected List<Vector3> readInPoints = new();


    public List<Vector3> ReadInPoints   // property
    {
        get { return readInPoints; }   // get method
        set { readInPoints = value; }  // set method
    }


    public abstract Task Parse(string path);


    /// <summary>
    /// Converts an string array into a vertex (float Vector3)
    /// </summary>
    /// <param name="sArray"></param>
    /// <returns></returns>
    protected Vector3 ArrayToVertex(string[] sArray)
    {
        Vector3 newVertex = new Vector3(
            float.Parse(sArray[0], CultureInfo.InvariantCulture),
        float.Parse(sArray[2], CultureInfo.InvariantCulture),
            float.Parse(sArray[1], CultureInfo.InvariantCulture));
        return newVertex;
    }

}

