﻿using System.IO;
using System.Threading.Tasks;

/// <summary>
/// Reads in .regele files which define locations of fibrosis
/// </summary>
class FibrosisElementParser : FibrosisParser
{
    public override async Task Parse(string path)
    {
        if (!path.EndsWith(".regele"))
        {
            StatusManager.Instance.SetStatus(path + "is not a .regele file for fibrosis data");
            return;
        }
        int counter = 0;
        StreamReader inp_stream = new(path);


        while (!inp_stream.EndOfStream)
        {
            string line = await inp_stream.ReadLineAsync();
            if (int.TryParse(line, out int triangle))
            {
                fibrosisElements.Add(triangle);
                ++counter;
            }

            //Yield to free cpu load
            if (counter % 100 == 0)
            {
                StatusManager.Instance.SetStatus( counter + " Fibrosis points read in");
                await Task.Yield();
            }
        }
    }
}

