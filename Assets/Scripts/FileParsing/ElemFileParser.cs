﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Parses in an .elem file into a list of ints which represents triangle connections
/// Result is stored in readInElements of the parent class
/// </summary>
public class ElemFileParser : ElementParser
{
    public ElemFileParser(List<string> allowedElements) : base(allowedElements)
    {
    }

    /// <summary>
    /// Takes a path and tries to parse this file into a List of triangle connections
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public async override Task Parse(string path)
    {
        int counter = 0;
        StreamReader inp_stream = new(path);
        while (!inp_stream.EndOfStream)
        {
            string line = inp_stream.ReadLine();
            //Data is separated by spaces therefore each part is separated into different strings
            string[] sArray = line.Split(' ');
            //Add only triangles
            if (line.StartsWith("Tr"))
            {
                List<int> realTriangle = ReadElemTriangle(sArray);
                //Only add triangle if it consists of three vertices
                if (realTriangle.Count == 3)
                {
                    readInElements.AddRange(realTriangle);

                    //Rotates the vertices to also draw the backside. Enables the mesh t be seen from all sides
                    //(Because of back-face culling. Turn of not possible otherwise normal vectors are wrong and lighting is not displayed correctly)
                    //Could be moved to an shader to improve performance
                    List<int> trianglRev = new()
                    {
                        realTriangle[0],
                        realTriangle[2],
                        realTriangle[1]
                    };
                    readInElements.AddRange(trianglRev);

                    //Logic for enabling the main thread to take control for other tasks to not block the main thread completely
                    //Also updates the current number of generated triangles on the status display
                    counter++;
                    if (counter % 100 == 0)
                    {
                        if (counter % 1000 == 0)
                        {
                            StatusManager.Instance.SetStatus(counter.ToString("#,##0") + " Triangles drawn");
                        }
                        await Task.Yield();
                    }


                }
            }
        }
    }

    /// <summary>
    /// Reads one triangle from a given .elem file
    /// </summary>
    /// <param name="sArray">Separated line of an .elem file</param>
    /// <returns></returns>
    private List<int> ReadElemTriangle(string[] sArray)
    {
        List<int> triangle = new();

        //Start with 1 because 0 is the definition (Tr for triangles)
        for (int i = 1; i < 4; i++)
        {
            //Prohibits spaces from ruining the code
            if (int.TryParse(sArray[i], out int output))
            {
                triangle.Add(output);
            }
        }

        //Filter only used triangles
        if (triangle.Count == 3 && IncludedInList(allowedElements, sArray[4]))//Regex.IsMatch( sArray[4],@"^[0-9]$"))
        {
            if (int.TryParse(sArray[4], out int readElementTag))
            {
                readInElementTags.Add(readElementTag);
            }
            else
            {
                Debug.LogError("Error passing " + sArray[4] + " as element tag");
            }

            return triangle;
        }
        //Return empty List if not full triangle was parsed
        return new List<int>();
    }

    /// <summary>
    /// Checks if a given string is in a list. 
    /// Used to filter strings according to the given list
    /// </summary>
    /// <param name="compareList">A list of strings which are white-listed</param>
    /// <param name="inputString">A given input string</param>
    /// <returns>True if string is in List</returns>
    private bool IncludedInList(List<string> compareList, string inputString)
    {
        foreach (String s in compareList)
        {
            if (s == inputString)
            {
                return true;
            }
        }
        return false;
    }
}

