using TMPro;
using UnityEngine;

/// <summary>
/// Updates the text of a given label for a specific PlayerPrefs topic
/// </summary>
public class UpdateTextFromTopic : MonoBehaviour
{
    public string topic;
    private TextMeshProUGUI m_TextMeshPro;
    // Start is called before the first frame update
    void Start()
    {
        m_TextMeshPro = GetComponent<TextMeshProUGUI>();
        UpdateText();
    }

    public void UpdateText()
    {
        m_TextMeshPro.text = PlayerPrefs.GetString(topic);
    }
}
