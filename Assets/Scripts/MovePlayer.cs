// Ignore Spelling: touchpad

using UnityEngine;
using Valve.VR;

/// <summary>
/// Script to move the player along the x and z axis (left/right, forward/backwards)
/// Must be added to th parent player object
/// </summary>
public class MovePlayer : MonoBehaviour
{
    public float movementSpeed = 0.1f; // Adjust this value to control movement speed
    public SteamVR_Action_Vector2 touchpadValue;
    public float deadSpaceX = 0.1f, deadSpaceY = 0.1f;// dead Space of the VR_Controler to avoid to slow movement



    /// <summary>
    /// Reads the current value of the touch-pad and moves the object around it's own coordinate space
    /// For this the SteamVR_ActionVector has to be put in manually selected and the action has to be defined
    /// in the Steam binding UI via track-pad->position "name of action"
    /// </summary>
    private void Update()
    {
        if (touchpadValue != null && touchpadValue.active)
        {
            float x_Val = touchpadValue.axis.x;
            float z_Val = touchpadValue.axis.y;
            if (Mathf.Abs(x_Val) > deadSpaceX || Mathf.Abs(z_Val) > deadSpaceY)
            {
                transform.localPosition += new Vector3(x_Val * movementSpeed, 0, z_Val * movementSpeed);
                //Debug.Log("Touch-pad x: " + x_Val);
            }
        }
    }
}
