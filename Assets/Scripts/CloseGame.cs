using UnityEngine;

/// <summary>
/// Used for exiting the program
/// </summary>
public class CloseGame : MonoBehaviour
{

    public void ExitProgram()
    {
        StatusManager.Instance.SetStatus("Closing program");
        Application.Quit();
    }
}
