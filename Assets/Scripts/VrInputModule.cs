using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;


/// <summary>
/// Handles the Input of the controllers for clicking and focusing on objects
/// </summary>
public class VrInputModule : BaseInputModule
{
    public Camera Camera;
    public SteamVR_Input_Sources m_TargetSource;
    public SteamVR_Action_Boolean m_ClickAction;

    private GameObject m_currentObject;
    private PointerEventData m_currentPointerEventData;

    protected override void Awake()
    {
        base.Awake();

        m_currentPointerEventData
            = new PointerEventData(eventSystem);
    }

    /// <summary>
    /// Inherited Method of BaseInputModel which handles every action
    /// </summary>
    public override void Process()
    {
        //Reset Data, set Camera
        m_currentPointerEventData.Reset();
        m_currentPointerEventData.position = new Vector2(Camera.pixelWidth / 2, Camera.pixelHeight / 2);

        //Raycast 
        eventSystem.RaycastAll(m_currentPointerEventData, m_RaycastResultCache);
        m_currentPointerEventData.pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        m_currentObject = m_currentPointerEventData.pointerCurrentRaycast.gameObject;

        //Clear Raycast
        m_RaycastResultCache.Clear();

        //Hover States for object
        HandlePointerExitAndEnter(m_currentPointerEventData, m_currentObject);

        //Press
        if (m_ClickAction.GetStateDown(m_TargetSource))
        {
            ProcessPress(m_currentPointerEventData);
        }


        //Release
        if (m_ClickAction.GetStateUp(m_TargetSource))
        {
            ProcessRelease(m_currentPointerEventData);
        }


    }


    /// <summary>
    /// Returns the event data of the pointer
    /// </summary>
    /// <returns></returns>
    public PointerEventData GetData()
    {
        return m_currentPointerEventData;
    }


    /// <summary>
    /// Method which handles the press of the pushed button
    /// </summary>
    private void ProcessPress(PointerEventData eventData)
    {
        //Set Ray-cast
        eventData.pointerPressRaycast = eventData.pointerCurrentRaycast;

        //Check for collision, get down handler, call
        GameObject newPointerPress = ExecuteEvents.ExecuteHierarchy(m_currentObject, eventData, ExecuteEvents.pointerDownHandler);
        //If no down handler, try get click handler
        if (newPointerPress == null)
        {
            newPointerPress = ExecuteEvents.GetEventHandler<IPointerClickHandler>(m_currentObject);
        }

        //Set data store clicked object
        eventData.pressPosition = eventData.position;
        eventData.pointerPress = newPointerPress;
        eventData.rawPointerPress = m_currentObject;

    }


    /// <summary>
    /// Method which handles the release of the pushed button
    /// </summary>
    /// <param name="eventData"></param>
    private void ProcessRelease(PointerEventData eventData)
    {
        //Execute pointer up
        ExecuteEvents.Execute(eventData.pointerPress, eventData, ExecuteEvents.pointerUpHandler);

        //Check for click handler
        GameObject pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(m_currentObject);

        //Check if matches for click

        if (eventData.pointerPress == pointerUpHandler)
        {
            ExecuteEvents.Execute(eventData.pointerPress, eventData, ExecuteEvents.pointerClickHandler);
        }

        //Clear selected gameObject
        eventSystem.SetSelectedGameObject(null);

        //Reset Data
        eventData.pressPosition = Vector2.zero;
        eventData.pointerPress = null;
        eventData.rawPointerPress = null;
    }
}
