using System;
using UnityEngine;

/// <summary>
/// Performance Test between MeshColor and Mesh Color Parallel
/// </summary>
public class TestPerformance : MonoBehaviour
{
    private MeshColor meshCol;
    private MeshColorParallel meshParal;
    public string path;
    // Start is called before the first frame update
    async void Start()
    {
        meshCol = gameObject.GetComponent<MeshColor>();
        meshParal = gameObject.GetComponent<MeshColorParallel>();
        DateTime now = DateTime.Now;
        await meshParal.readAnimationFile(path);
        Debug.Log("Parallel has taken" + DateTime.Now.Subtract(now).Milliseconds);
        now = DateTime.Now;

        await meshCol.ReadAnimationFile(path);
        Debug.Log("Non Parallel has taken" + DateTime.Now.Subtract(now).Milliseconds);
    }
}
