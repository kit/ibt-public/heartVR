﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    internal static class HelperMethods
    {
        public static List<Vector2> InitZeroV2Array(int length)
        {
            List<Vector2> vector2 = new List<Vector2>();
            for (int i = 0; i < length; i++)
            {
                vector2.Add(new Vector2(0, 0));
            }
            return vector2;
        }
    }
}
