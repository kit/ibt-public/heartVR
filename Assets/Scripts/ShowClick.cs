using UnityEngine;


/// <summary>
/// Debug scrip witch prints its name if clicked
/// </summary>
public class ShowClick : MonoBehaviour
{
    public void Click()
    {
        Debug.Log("Clicked object: " + gameObject.name);
    }
}
