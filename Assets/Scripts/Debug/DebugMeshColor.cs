using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMeshColor : MonoBehaviour
{
    MeshColor meshColorScript;
    private int currentTriangleIndex = 0;
    private Mesh mesh;
    // Start is called before the first frame update
    async void Start()
    {
        //initDebugMesh();
    }

    public void initDebugMesh()
    {
        meshColorScript = GetComponent<MeshColor>();
        mesh = GetComponent<MeshFilter>().mesh;
        Color[] meshColors = new Color[mesh.vertices.Length];
        for (int i = 0; i < meshColors.Length; i++)
        {
            meshColors[i] = Color.Lerp(Color.blue, Color.blue, (float)i / mesh.vertices.Length);
            //meshColorScript.PaintTriangle(i);
        }

        InvokeRepeating(nameof(animateTriangle), 1, 1);

        mesh.colors = meshColors;
        meshColorScript.StoredMesh = mesh;

        // await meshColorScript.generateFibrosis();
    }

    private void animateTriangle()
    {
        if (currentTriangleIndex >= mesh.triangles.Length)
        {
            currentTriangleIndex = 0;
        }
        StatusManager.Instance.SetStatus("Colored Triangle" + currentTriangleIndex);
        //PaintTriangle(Mathf.Abs((currentTriangleIndex - 1) % mesh.triangles.Length), Color.blue);

        PaintTriangle(currentTriangleIndex, Color.red);

        currentTriangleIndex++;
    }

    public void PaintTriangle(int triangleIndex, Color color)
    {
        int vertex0 = mesh.triangles[triangleIndex * 3];
        int vertex1 = mesh.triangles[triangleIndex * 3 + 1];
        int vertex2 = mesh.triangles[triangleIndex * 3 + 2];

        PaintVertices(new int[] { vertex0, vertex1, vertex2 }, color);
    }

    private void PaintVertices(int[] vertices, Color color)
    {
        Color[] colors = mesh.colors;
        foreach (int vertex in vertices)
        {
            colors[vertex] = color;
        }
        mesh.colors = colors;
    }
}
