using UnityEngine;


/// <summary>
/// Deactivates the given object on start and activates an Canvas (if exists) if it was disabled in the Editor, so that it is loaded but not active
/// </summary>
public class DeactivateOnStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (TryGetComponent<Canvas>(out var canvas))
        {
            canvas.enabled = true;
        }

        this.gameObject.SetActive(false);
    }
}
