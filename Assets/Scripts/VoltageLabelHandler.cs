using TMPro;
using UnityEngine;

/// <summary>
/// Updates min max and mid voltage labels for given values
/// </summary>
public class VoltageLabelHandler : MonoBehaviour
{
    public TextMeshProUGUI labelMin, labelMid, labelMax;
    public static VoltageLabelHandler Instance { get; private set; }

    /// <summary>
    /// Singleton to avoid unnecessary links
    /// </summary>
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Updates the min, mid, max label to the corresponding voltage values given in mV
    /// </summary>
    /// <param name="min"></param>
    /// <param name="mid"></param>
    /// <param name="max"></param>
    public void UpdateMinMidMax(float min, float mid, float max)
    {
        if (labelMin != null && labelMid != null && labelMax != null)
        {
            labelMin.text = "Min: " + min.ToString("0.00",System.Globalization.CultureInfo.InvariantCulture) + " mV";
            labelMid.text = "Mid: " + mid.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + " mV";
            labelMax.text = "Max: " + max.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) + " mV";
        }

    }
}
