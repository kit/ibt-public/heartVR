using TMPro;
using UnityEngine;

public class SelectedValueManager : MonoBehaviour
{
    // Start is called before the first frame update
    public string topic;


    public void changeValue()
    {
        TextMeshProUGUI textNameField = this.GetComponentInChildren<TextMeshProUGUI>();
        PlayerPrefs.SetString(topic, textNameField.text);
        PlayerPrefs.Save();

    }

}
