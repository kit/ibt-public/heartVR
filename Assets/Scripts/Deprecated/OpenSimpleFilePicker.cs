using SimpleFileBrowser;
using UnityEngine;

public class OpenSimpleFilePicker : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public void openSimpleFilePicker()
    {
        FileBrowser.ShowLoadDialog((paths) => { Debug.Log("Selected: " + paths[0]); },
                                   () => { Debug.Log("Canceled"); },
                                   FileBrowser.PickMode.Folders, false, null, null, "Select Folder", "Select");

    }

    // Update is called once per frame
    void Update()
    {

    }
}
