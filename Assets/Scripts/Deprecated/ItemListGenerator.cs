using UnityEngine;

public class ItemListGenerator : MonoBehaviour
{
    public GameObject buttonItem;
    public GameObject itemListCanvas;
    public float sizeOfItemFrame = 0.5f;
    public string contentTopic;

    public ItemListGenerator(GameObject buttonItem, float sizeOfItemFrame = 0.5f)
    {
        this.buttonItem = buttonItem;
        this.sizeOfItemFrame = sizeOfItemFrame;
    }


    public void GenerateList(string[] itemList)
    {
        int numOfspawenedObjects = 0;
        foreach (string item in itemList)
        {
            GameObject spawnedObject = Instantiate(buttonItem, itemListCanvas.transform.position, Quaternion.identity);
            spawnedObject.transform.parent = itemListCanvas.transform;

            spawnedObject.transform.SetLocalPositionAndRotation(new Vector3(0, -numOfspawenedObjects * sizeOfItemFrame, 0), Quaternion.identity);
            ListItemManager currentListManager = spawnedObject.GetComponent<ListItemManager>();
            //Setting topic to store the selected value
            spawnedObject.GetComponent<SelectedValueManager>().topic = contentTopic;
            currentListManager.Start();
            currentListManager.UpdateName(item);
            numOfspawenedObjects++;

        }
        itemListCanvas.GetComponent<Canvas>().enabled = true;
    }
}
