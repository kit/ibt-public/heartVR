using System.IO;
using System.Threading.Tasks;
using TMPro;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;


/// <summary>
/// Experiment to make MeshColor parallel 
/// !!!!Currently not working!!!
/// </summary>
public class MeshColorParallel : MonoBehaviour
{
    public int numberOfSteps = 100;
    public float repeatingTime = 0.1f;
    public Color minColor = Color.red, maxColor = Color.blue;
    public Color[] colTestArr;
    public GameObject[] colorCubes;
    public TextMeshProUGUI statusLabel;
    private int colorIndex = 0;
    private Color[][] colorArray;
    private Mesh storedMesh;
    private float[,] voltages;
    private float maxVoltage = float.MinValue;
    private float minVoltage = float.MaxValue;

    /// <summary>
    /// Calculates an animation pattern from y_min to y_max of the given mesh. 
    /// Uses the number of steps given by the class variable numberOfSteps. 
    /// Divides the mesh into "numberOfStep" parts (depending on y position) and highlights each segment sequentially,
    /// generating a flow from y_min to y_max.
    /// </summary>
    /// <param name="mesh">Mesh that is animated</param>
    /// <returns>Task object for async handling</returns>
    public async Task calculateAndStartColoranimamation(Mesh mesh)
    {
        initColorArray(mesh.vertices.Length);
        await generateColorSequence(mesh);
        InvokeRepeating(nameof(showColorAnimation), 1, repeatingTime);
        print("Started Color Animation");
    }
    /// <summary>
    /// Initializes the color array as an jacked array with the size [numberOfSteps][numVertices]
    /// With numberOfSteps is an variable from this class.
    /// </summary>
    /// <param name="numVertices">Number of vertices of the mesh which is animated</param>
    private void initColorArray(int numVertices)
    {
        colorArray = new Color[numberOfSteps][];
        for (int i = 0; i < numberOfSteps; i++)
        {
            colorArray[i] = new Color[numVertices];
        }
    }

    /// <summary>
    /// Generates an Color sequence which divides the given mesh into numberOfSteps parts according to the y value of each vertex.
    /// Leads to an animation where each segment is highlighted (with a red color) sequentially. 
    /// </summary>
    /// <param name="mesh">The mesh which is animated</param>
    /// <returns>Task object to handle the async call</returns>
    private async Task generateColorSequence(Mesh mesh)
    {
        Debug.Log("Update Mesh color started");

        mesh.RecalculateBounds();
        Vector3 minVer = mesh.bounds.min;
        Vector3 maxVer = mesh.bounds.max;

        mesh.colors = new Color[mesh.vertices.Length];
        Vector3 vectorOffset = (maxVer - minVer);
        storedMesh = mesh;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Vector3 offsetCorVec = mesh.vertices[i] - minVer;
            //Index to which segment the point belongs
            int highlightIndex = (int)(offsetCorVec.z / ((maxVer.z - minVer.z) / numberOfSteps));
            for (int j = 0; j < numberOfSteps; j++)
            {
                if (j == highlightIndex)
                {
                    colorArray[j][i] = new Color(1, 0, 0);
                }
                else
                {
                    colorArray[j][i] = new Color(0, 0, 1);
                }
            }
            //Process in Blocks of 1000
            if (i % 1000 == 0)
            {
                await Task.Yield();
            }

        }
        statusLabel.text = "Animation calculated";
        Debug.Log("Animation calculated");
    }

    /// <summary>
    /// Function which can be called repeating to create an animation between the steps in the colorArray
    /// Uses the colorIndex Integer for storing the current time index of the animation
    /// </summary>
    public void showColorAnimation()
    {

        storedMesh.colors = colorArray[colorIndex];

        Color.RGBToHSV(colorArray[colorIndex][100], out var hue, out var sat, out var val);
        //Debug.Log("Current t: " + hue);


        colorIndex++;
        if (colorIndex >= numberOfSteps)
        {
            colorIndex = 0;
        }

    }

    public async Task readAnimationFile(string path)
    {
        Debug.Log("Started reading the animation file");
        string[] textFile = File.ReadAllLines(path);



        numberOfSteps = textFile[0].Split(' ').Length;
        //NativeArray<byte> textFile;
        ReadInFileJob job = new ReadInFileJob();
        NativeArray<float> jobVoltages = new NativeArray<float>(2, Allocator.TempJob);
        job.inputFile = textFile;
        job.numberOfSteps = numberOfSteps;
        job.voltages = jobVoltages;
        JobHandle handle = job.Schedule(textFile.Length, 1);

        handle.Complete();


        voltages = new float[numberOfSteps, textFile.Length];
        for (int i = 0; i < textFile.Length; i++)
        {
            /*string[] line = textFile[i].Split(' ');
            for (int j = 0; j < numberOfSteps; j++)
            {
                float voltage = float.Parse(line[j]);
                maxVoltage = Mathf.Max(voltage, maxVoltage);
                minVoltage = Mathf.Min(voltage, minVoltage);
                voltages[j, i] = voltage;
            }*/
            if (i % 200 == 0)
            {
                await Task.Yield();
                if (i % 1000 == 0)
                {
                    statusLabel.text = i.ToString("#,##0") + " number of animation points read in";
                }
            }
        }
        statusLabel.text = "Finished reading the animation file";
        Debug.Log("Finished reading the animation file");
        Debug.Log("Min Voltage: " + minVoltage);
        Debug.Log("Max Voltage: " + maxVoltage);
    }


    public async Task createAnimationForIGBFile(Mesh mesh)
    {
        storedMesh = mesh;
        Debug.Log("Started interpolating colors");
        statusLabel.text = "Started calculating color values";
        initColorArray(voltages.GetLength(1));
        //GetLength(1) returns the number of vertices
        for (int i = 0; i < voltages.GetLength(1); i++)
        {
            for (int j = 0; j < numberOfSteps; j++)
            {


                //Color.Lerp(oldColor, newColor, t);
                colorArray[j][i] = calculateColor(voltages[j, i]);
                if (i > 100 && i < 4000)
                {
                    Color.RGBToHSV(colorArray[j][i], out float H, out float S, out float V);
                    colorArray[j][i] = Color.HSVToRGB(H, 0.8f, 1f);
                }
            }
            //print(i);
            if (i % 1000 == 0)
            {
                statusLabel.text = i.ToString("#,##0") + " from " + mesh.vertices.Length.ToString("#,##0") + " colors calculated";
                await Task.Yield();

            }
        }
        ColorReferenceCubes();
        Debug.Log("Finished interpolating colors");
        statusLabel.text = "Finished calculating color values";
        InvokeRepeating(nameof(showColorAnimation), 1, repeatingTime);

    }

    private Color calculateColor(float value)
    {
        float t = Mathf.InverseLerp(minVoltage, maxVoltage, value);

        //Color oldColor = colTestArr[(int)t];
        // Color newColor = colTestArr[(int)(t + 1f)];

        return Color.HSVToRGB(Mathf.Clamp(0.7f - t, 0, 1), 1, 1);
    }

    /// <summary>
    /// Takes the 3 reference cubes to show the color spectrum
    /// </summary>
    private void ColorReferenceCubes()
    {
        colorCubes[0].GetComponent<MeshRenderer>().material.color = calculateColor(minVoltage);
        colorCubes[1].GetComponent<MeshRenderer>().material.color = calculateColor((maxVoltage - minVoltage) / 2 + minVoltage);
        colorCubes[2].GetComponent<MeshRenderer>().material.color = calculateColor(maxVoltage);
    }

    /// <summary>
    /// Colors the mesh relative to the position of each vertex, meaning that 
    /// </summary>
    /// <param name="mesh"></param>
    /// <returns></returns>
    public async Task updateMeshColor(Mesh mesh)
    {
        print("Update Mesh color started");
        Vector3 minVer = mesh.bounds.min;
        Vector3 maxVer = mesh.bounds.max;
        Color[] colors = new Color[mesh.vertices.Length];
        Vector3 vectorOffset = (maxVer - minVer);
        Vector3 scaleVert = new Vector3(1 / vectorOffset.x, 1 / vectorOffset.y, 1 / vectorOffset.z);
        for (int i = 0; i < colors.Length; i++)
        {
            Vector3 offsetCorVec = mesh.vertices[i] - minVer;
            float r, g, b;
            r = offsetCorVec.x * scaleVert.x;
            g = offsetCorVec.y * scaleVert.y;
            b = offsetCorVec.z * scaleVert.z;
            //colors[i] = new Color(255,0,0);
            colors[i] = new Color(r, g, b);
            //Process in Blocks of 1000
            if (i % 1000 == 0)
            {
                //print("ping: " + i);
                await Task.Yield();
            }

        }
        mesh.colors = colors;
        print("New Mesh Color added");
    }
}


public struct ReadInFileJob : IJobParallelFor
{
    public int numberOfSteps;
    public string[] inputFile;
    //public NativeArray<string> strings;
    public NativeArray<float> maxVoltage;
    public NativeArray<float> minVoltage;
    public NativeArray<float> voltages;
    public void Execute(int index)
    {
        string[] line = inputFile[index].Split(' ');
        for (int j = 0; j < numberOfSteps; j++)
        {
            float voltage = float.Parse(line[j]);
            maxVoltage[index] = Mathf.Max(voltage, maxVoltage[index]);
            minVoltage[index] = Mathf.Min(voltage, minVoltage[index]);
            voltages[index] = voltage;
        }
    }
}
