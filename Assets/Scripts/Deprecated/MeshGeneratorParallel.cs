using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshGeneratorParallel : MonoBehaviour
{
    // Start is called before the first frame update
    public string filename = "Data/";
    public string voltageDataPath;
    public int scale = 100000;
    public TextMeshProUGUI statusLabel;
    Mesh mesh;
    List<Vector3> vertices = new();
    List<int> triangles = new();
    int offsetCounter = 0;
    int lastOffset = 0;
    readonly List<String> allowedElements = new() { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
    private Vector3 minPos, maxPos;

    async void Start()
    {
        UpdatePaths();
        //await updateAndGenerateNewMesh();
    }

    public async Task UpdateMeshAndAnimation()
    {
        if (PlayerPrefs.HasKey(Constants.dataDirectory) && PlayerPrefs.HasKey(Constants.animationFilePath))
        {

            await UpdateAndGenerateNewMesh();
        }
        else
        {
            Debug.Log("No path to animation and mesh data set");
        }

    }
    public async Task UpdateAnimation()
    {
        UpdatePaths();
        MeshColor colorGen = GetComponent<MeshColor>();
        Debug.Log("Started Reading elements from " + voltageDataPath);
        await colorGen.ReadAnimationFile(voltageDataPath);
        Debug.Log("Finished reading elem file; starting processing");
        await colorGen.CreateAnimationForIGBFile(mesh);
        Debug.Log("Finished Calculating Colors");
    }

    /// <summary>
    /// Updates the currently stored paths for the animation files from the PlayerPrefs storage into the locally used variables
    /// </summary>
    public void UpdatePaths()
    {
        filename = PlayerPrefs.GetString(Constants.dataDirectory, filename);
        voltageDataPath = PlayerPrefs.GetString(Constants.animationFilePath, voltageDataPath);
    }

    private async Task UpdateAndGenerateNewMesh()
    {
        UpdatePaths();

        minPos = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        maxPos = new Vector3(float.MinValue, float.MinValue, float.MinValue);
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        //CreateShape();
        if (filename.EndsWith(".vtk"))
        {
            ReadVTKFile();
        }
        else
        {
            await ReadCarpFolder();
        }

        UpdateMesh();
        //MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        //meshRenderer.material = new Material(Shader.Find("Standard"));

        //await colorGen.calculateAndStartColoranimamation(mesh);
        Bounds meshBounds = mesh.bounds;
        statusLabel.text = "Updating bounds";
        UpdateCenter(meshBounds);
        statusLabel.text = "Finished updating bounds";
        await UpdateAnimation();

    }
    private async Task ReadCarpFolder()
    {
        foreach (var file in new DirectoryInfo(filename).GetFiles())
        {
            if (file.Name.EndsWith(".pts"))
            {
                await ParsePTSFile(file.FullName);

            }
            else if (file.Name.EndsWith(".elem"))
            {
                await ParseElemFile(file.FullName);
            }
        }
    }

    /// <summary>
    /// Moves the mesh to move the pivot-point of the mesh to the mid point of the mesh
    /// Allows for better rotation
    /// </summary>
    /// <param name="meshBounds">Bounds of the aabb of the mesh</param>
    private void UpdateCenter(Bounds meshBounds)
    {
        Vector3 scaleFactor = new(-0.5f, -0.5f, -0.5f);


        transform.localPosition = Vector3.Scale((meshBounds.max - meshBounds.min), scaleFactor) - meshBounds.min;
    }

    private async Task ParseElemFile(string path)
    {
        int counter = 0;
        StreamReader inp_stream = new(path);
        while (!inp_stream.EndOfStream)
        {
            string line = inp_stream.ReadLine();
            string[] sArray = line.Split(' ');
            if (line.StartsWith("Tr"))
            {
                List<int> realTriangle = ReadElemTriangle(sArray);
                if (realTriangle.Count == 3)
                {


                    triangles.AddRange(realTriangle);
                    List<int> trianglRev = new()
                    {
                        realTriangle[0],
                        realTriangle[2],
                        realTriangle[1]
                    };

                    triangles.AddRange(trianglRev);
                    counter++;
                    if (counter % 100 == 0)
                    {
                        if (counter % 1000 == 0)
                        {
                            Debug.Log(counter.ToString("#,##0") + " Triangles drawn");
                            statusLabel.text = (counter.ToString("#,##0") + " Triangles drawn");
                        }
                        await Task.Yield();
                    }


                }
            }
        }
    }
    private List<int> ReadElemTriangle(string[] sArray)
    {
        List<int> triangle = new();

        for (int i = 1; i < 4; i++)
        {

            //Prohibits spaces from ruining the code
            if (int.TryParse(sArray[i], out int output))
            {
                triangle.Add(output);
            }
        }

        if (triangle.Count == 3 && IncludedInList(allowedElements, sArray[4]))//Regex.IsMatch( sArray[4],@"^[0-9]$"))
        {
            return triangle;
        }
        //Return empty List if not full triangle was parsed
        return new List<int>();
    }
    private async Task ParsePTSFile(string path)
    {
        int counter = 0;
        StreamReader inp_stream = new(path);
        while (!inp_stream.EndOfStream)
        {
            string line = inp_stream.ReadLine();
            string[] sArray = line.Split(' ');
            if (sArray.Length == 3)
            {
                vertices.Add(ArrayToVertex(sArray));
                counter++;
                if (counter % 100 == 0)
                {
                    if (counter % 1000 == 0)
                    {
                        Debug.Log(counter.ToString("#,##0") + " Points added");
                        statusLabel.text = counter.ToString("#,##0") + " Points added";
                    }
                    await Task.Yield();
                }
            }
        }
    }


    void UpdateMesh()
    {
        statusLabel.text = "Start updating mesh";
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        print("Number of Vertices: " + vertices.Count());
        print("Number of Triangles: " + triangles.Count / 3);
        statusLabel.text = "Finished updating mesh";
        //mesh.RecalculateNormals();
        //mesh.RecalculateBounds();
    }

    private void ReadVTKFile()
    {
        bool points = false;
        bool types = false;
        bool cells = false;
        bool connectivity = false;
        bool offsets = false;
        int type5Count = 0;
        StreamReader inp_stream = new(filename);
        Debug.Log("Reading mesh file " + filename);
        while (!inp_stream.EndOfStream)
        {
            string line = inp_stream.ReadLine();

            if (line == "" || char.IsLetter(line[0]))
            {
                points = types = cells = connectivity = offsets = false;
            }
            if (line.StartsWith("POINTS"))
            {
                points = true;
            }
            else if (line.StartsWith("CELL_TYPES"))
            {
                points = false;
                types = true;
            }
            else if (line.StartsWith("CELLS"))
            {
                cells = true;
                types = points = false;
                print("The number of defined Triangles is: " + type5Count);
            }
            else if (line.StartsWith("CELL_DATA"))
            {
                cells = false;
            }
            else if (line.StartsWith("CONNECTIVITY"))
            {
                connectivity = true;
                types = points = false;
                print("Number of offsets is" + offsetCounter);
            }
            else if (line.StartsWith("OFFSETS"))
            {
                offsets = true;
            }


            if (points)
            {
                //Check if line starts with a number
                if (line.Length > 5 && (char.IsDigit(line[0]) || line[0].Equals('-')))
                {
                    string[] sArray = line.Split(' ');
                    if (sArray.Length < 6)
                    {
                        vertices.Add(ArrayToVertex(sArray));
                    }
                    else
                    {
                        vertices.Add(ArrayToVertex(sArray[0..3]));
                        if (sArray.Length >= 6)
                        {
                            vertices.Add(ArrayToVertex(sArray[3..6]));
                        }
                        if (sArray.Length >= 9)
                            vertices.Add(ArrayToVertex(sArray[6..9]));
                    }
                }
            }
            else if (types)
            {
                if (line.Length > 0 && line[0] == '5')
                {
                    type5Count++;
                }
            }
            else if (cells || connectivity)
            {
                if (line.StartsWith("OFFSETS"))
                {
                    //Only offset definition, use CONNECTIVITY Field
                    cells = false;
                }
                string[] sArray = line.Split(' ');
                //Render only triangles to work with meshes
                List<int> triangle = ArrayToTriangleVTK(sArray);
                if (triangle.Count % 3 != 0)
                {
                    //Triangles have to have 3 vertices
                    print("No real triangle at input: " + line);

                }
                else
                {
                    //concatenate Lists
                    triangles.AddRange(triangle);
                }

            }
            else if (offsets)
            {
                HandleOffsets(line.Split(' '));
            }
        }
        inp_stream.Close();
    }

    void HandleOffsets(string[] sArray)
    {
        foreach (string s in sArray)
        {
            if (s != "")
            {
                if (int.TryParse(s, out int output))
                {
                    if (output - lastOffset != 3)
                    {
                        print("Offset difference was not 3" + sArray);
                    }
                    lastOffset = output;
                    offsetCounter++;
                    if (output % 3 != 0)
                    {
                        print(sArray);
                    }
                }
            }
        }
    }


    Vector3 ArrayToVertex(string[] sArray)
    {
        Vector3 newVertex = new Vector3(
            -float.Parse(sArray[0], CultureInfo.InvariantCulture),
            float.Parse(sArray[2], CultureInfo.InvariantCulture),
            float.Parse(sArray[1], CultureInfo.InvariantCulture)) / scale;
        minPos = Vector3.Min(newVertex, minPos);
        maxPos = Vector3.Max(newVertex, maxPos);
        return newVertex;
    }

    List<int> ArrayToTriangleVTK(string[] sArray)
    {
        //Old Notation
        if (sArray[0].StartsWith("3") && sArray.Length < 5)
        {
            //Removes first number, because this is the number of elements of an shape
            sArray = sArray.Skip(1).ToArray();
            List<int> triangle = new();
            for (int i = 0; i < 3; i++)
            {

                //Prohibits spaces from ruining the code
                if (int.TryParse(sArray[i], out int output))
                {
                    triangle.Add(output);
                }
            }
            return triangle;
        }
        else
        {
            //Multi-cell data
            List<int> triangle = new();
            foreach (String s in sArray)
            {
                if (s == "")
                {
                    continue;
                    //Skip if s is empty
                }
                if (int.TryParse(s, out int output))
                {
                    triangle.Add(output);
                }
                else
                {
                    print("Parsing: " + s + " as triangle vertex was not possible");
                }

            }
            return triangle;
        }

    }

    private bool IncludedInList(List<string> compareList, string inputString)
    {
        foreach (String s in compareList)
        {
            if (s == inputString)
            {
                return true;
            }
        }
        return false;

    }
}
