using TMPro;
using UnityEngine;

public class ListItemManager : MonoBehaviour
{
    private string nameOfFolder = "";
    private TextMeshProUGUI textNameField;

    public void Start()
    {
        textNameField = this.GetComponentInChildren<TextMeshProUGUI>();
        //updateName("Test");
    }



    public void UpdateName(string name)
    {
        nameOfFolder = name;
        textNameField.text = name;

    }

}
