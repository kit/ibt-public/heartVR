using UnityEngine;
using Valve.VR;

/// <summary>
/// Handles the rotation of the attached object around the world axis
/// </summary>
public class RotateWithController : MonoBehaviour
{
    public float rotationSpeed = 1.0f; // Adjust this value to control rotation speed
    public SteamVR_Action_Vector2 touchPadValue;
    public float deadSpaceX = 0.1f, deadSpaceY = 0.1f;// dead Space of the VR_Controler to avoid to slow movement

    /// <summary>
    /// Reads the current value of the touch-pad and rotates the object around the world axis
    /// For this the SteamVR_ActionVector has to be put in manually selected and the action has to be defined
    /// in the Steam binding UI via track-pad->position "name of action"
    /// </summary>
    private void Update()
    {
        if (touchPadValue != null && touchPadValue.active)
        {
            float x_Val = touchPadValue.axis.x;
            float y_Val = touchPadValue.axis.y;
            if (Mathf.Abs(x_Val) > deadSpaceX || Mathf.Abs(y_Val) > deadSpaceY)
            {
                transform.Rotate(transform.InverseTransformVector(Vector3.up), -x_Val * rotationSpeed);
                transform.Rotate(transform.InverseTransformVector(Vector3.right), y_Val * rotationSpeed);
                //print("Touch-pad x: " + x_Val);
            }
        }
    }
}
