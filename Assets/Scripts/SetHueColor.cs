using UnityEngine;

/// <summary>
/// Changes the color of the object to given HSV Values
/// </summary>
public class SetHueColor : MonoBehaviour
{
    public float Hue = 0;
    public float Sat = 1;
    public float Tone = 1;
    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer currentMeshRenderer = GetComponent<MeshRenderer>();
        currentMeshRenderer.material.color = Color.HSVToRGB(Hue, Sat, Tone);
    }

}
