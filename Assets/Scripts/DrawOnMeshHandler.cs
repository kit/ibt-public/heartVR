using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;


/// <summary>
/// Manage the handling of marking ablated tissue. 
/// Uses a given button on the controller (default: grip button) to enable (pushed) disable (release) the marking
/// </summary>
public class DrawOnMeshHandler : MonoBehaviour
{
    public Camera Camera;//Camera from which the RayCast is calculated, should be one of the dummy cameras in the controller prefabs
    public MeshColor meshCol;//Mesh-color script to change the color of the marked sections
    public SteamVR_Action_Boolean grabGrip;//Variable which changes to true and triggers an event if the controller button is pressed
    private bool draw;//true if  drawing is active
    private bool errase = false; //Switches the erase mode to remove markings

    public Button switchButton;
    //Colors to change the Button if pressed
    public Color buttonActive;
    public Color buttonHoverActive;
    public Color buttonPressedActive;

    public Color buttonInactive;
    public Color buttonHoverInactive;
    public Color buttonPressedInactive;


    // Add Listeners to react to button presses
    void Start()
    {
        grabGrip.AddOnStateDownListener(TriggerDown, SteamVR_Input_Sources.RightHand);
        grabGrip.AddOnStateUpListener(TriggerUp, SteamVR_Input_Sources.RightHand);
    }

    /// <summary>
    /// Handler method for button down events. Switches the drawing bool to true to enable drawing in the Update method
    /// </summary>
    /// <param name="grabGrip"></param>
    /// <param name="fromSource"></param>
    public void TriggerDown(SteamVR_Action_Boolean grabGrip, SteamVR_Input_Sources fromSource)
    {
        draw = true;
        Debug.Log("Start Drawing");
    }

    /// <summary>
    /// Handler method for button up events. Switches the drawing bool to false to disable drawing in the Update method
    /// </summary>
    /// <param name="grabGrip"></param>
    /// <param name="fromSource"></param>
    public void TriggerUp(SteamVR_Action_Boolean grabGrip, SteamVR_Input_Sources fromSource)
    {
        draw = false;
        Debug.Log("End Drawing");
    }

    public void StoreData()
    {
        List<float> markings = meshCol.getMarkings();
        string storagePath = PlayerPrefs.GetString(Constants.drawingFilePath) + "/drawing.txt";
        using (StreamWriter writer = new StreamWriter(storagePath))
        {
            foreach (var marking in markings)
            {
                writer.WriteLine(marking);
            }

        }

        StatusManager.Instance.SetStatus("Stored markings at " + storagePath);

    }

    public void LoadData()
    {
        List<float> markings = new List<float>();
        string storagePath = PlayerPrefs.GetString(Constants.drawingFilePath) + "/drawing.txt";
        using (StreamReader reader = new StreamReader(storagePath))
        {
            while (!reader.EndOfStream)
            {
                markings.Add(float.Parse(reader.ReadLine()));
            }
        }
        meshCol.setMarkings(markings);

        StatusManager.Instance.SetStatus("Opened markings from " + storagePath);

    }

    /// <summary>
    /// Flips the erase variable to enable/disable the erase mode
    /// Also changes the color theme of the corresponding button
    /// </summary>
    public void SwitchEraseOnOff()
    {
        ColorBlock colors = switchButton.colors;

        errase = !errase;
        if (errase)
        {
            colors.highlightedColor = buttonHoverActive;
            colors.pressedColor = buttonPressedActive;
            colors.normalColor = buttonActive;
        }
        else
        {
            colors.highlightedColor = buttonHoverInactive;
            colors.pressedColor = buttonPressedInactive;
            colors.normalColor = buttonInactive;
        }
        switchButton.colors = colors;
    }

    // Checks if drawing is active. If so the mesh color script is called to change the vertex colors at which the ray from the selected hand is shot
    void Update()
    {
        if (draw)
        {
            Ray ray = new(Camera.gameObject.transform.position, Camera.gameObject.transform.forward);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.transform == this.transform)
                {
                    if (errase)
                    {
                        meshCol.ResetOneTriangle(hit.triangleIndex);
                    }
                    else
                    {
                        meshCol.PaintTriangle(hit.triangleIndex, meshCol.markingColor);
                    }
                    Debug.Log("Triangle index that was hit: " + hit.triangleIndex);
                }
            }
        }
    }
}
