using TMPro;
using UnityEngine;

/// <summary>
/// Singleton class used to display status messages in VR and on the console
/// </summary>
public class StatusManager : MonoBehaviour
{
    public static StatusManager Instance { get; private set; }
    public TextMeshProUGUI statusText;//Panel at which the message is displayed

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Sets the given status message to the status panel also prints it to the debug log and console
    /// </summary>
    /// <param name="message">Message which will be displayed</param>
    public void SetStatus(string message)
    {
        Debug.Log(message);
        statusText.text = message;
    }


    /// <summary>
    /// Clears the status
    /// </summary>
    public void ClearStatus()
    {
        statusText.text = "";
    }
}
