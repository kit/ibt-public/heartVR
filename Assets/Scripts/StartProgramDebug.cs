using System.Threading.Tasks;
using UnityEngine;


/// <summary>
/// Starts Rendering of the stored mesh for debugging without button pressed
/// </summary>
public class StartProgramDebug : MonoBehaviour
{
    public MeshGenerator meshGen;
    void Start()
    {
        if (Debug.isDebugBuild)
        {
            Invoke(nameof(StartProgram), 3f);
        }
    }

    async Task StartProgram()
    {
        await meshGen.UpdateMeshAndAnimation();
    }
}
