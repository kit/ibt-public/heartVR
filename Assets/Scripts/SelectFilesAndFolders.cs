using SimpleFileBrowser;
using UnityEngine;


/// <summary>
/// Class to handle the selection for files and folders of the project.
/// Therefore SimpleFileBrowser https://github.com/yasirkula/UnitySimpleFileBrowser was used
/// </summary>
public class SelectFilesAndFolders : MonoBehaviour
{
    //Key for the PlayerPref to store the selected paths
    public string storedTopicKey;
    //label to display the current value of the assigned topic
    public UpdateTextFromTopic TextLabel;
    //allowed file types (does not influence the selection of folders)
    public string fileType = ".igb";

    /// <summary>
    /// Opens the SimpleFile Browser and only allows the selection of folders
    /// Selected files are processed in handleSelectedFolder()
    /// </summary>
    public void SelectFolders()
    {
        FileBrowser.ShowLoadDialog((paths) => { HandleSelectedFolder(paths); },
                           () => { Debug.Log("Canceled"); },
                           FileBrowser.PickMode.Folders, false, null, null, "Select Data Folder", "Select");
    }


    /// <summary>
    /// Opens the SimpleFile Browser and only allows the selection of files with the given file extension (fileType)
    /// Selected files are processed in handleSelectedFiles()
    /// </summary>
    public void SelectFiles()
    {
        //Updates the Filters to only allow the selected file type
        FileBrowser.SetFilters(true, new FileBrowser.Filter("IGB", ".igb"));
        FileBrowser.SetDefaultFilter(fileType);
        //Open File-browser and get the paths
        FileBrowser.ShowLoadDialog((paths) => { HandleSelectedFiles(paths); },
                          () => { Debug.Log("Canceled"); },
                          FileBrowser.PickMode.Files, false, null, null, "Select Animation Files", "Select");
    }

    /// <summary>
    /// Handles the selected pathnames and updates the PlayerPrefs to store the selected files for the program on disk
    /// </summary>
    /// <param name="paths">Selected paths (should be only one, but can handle also multiple)</param>
    private void HandleSelectedFolder(string[] paths)
    {
        //Storing of the paths
        PlayerPrefs.SetString(storedTopicKey, paths[0]);
        PlayerPrefs.Save();
        Debug.Log("Selected Paths are: " + paths[0].ToString() + ". Stored at playerPref" + storedTopicKey);

        //Update label for the corresponding button
        TextLabel.UpdateText();
    }


    /// <summary>
    /// Handles the selected pathnames and updates the PlayerPrefs to store the selected folders for the program on disk
    /// </summary>
    /// <param name="paths">Selected paths (should be only one, but can handle also multiple)</param>
    private void HandleSelectedFiles(string[] paths)
    {
        PlayerPrefs.SetString(Constants.GetPathKey(storedTopicKey), paths[0]);
        PlayerPrefs.Save();
        Debug.Log("Selected Files are: " + paths.ToString());

        //Update label for the corresponding button
        TextLabel.UpdateText();
    }
}
