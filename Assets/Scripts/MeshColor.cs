using Assets.Scripts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public class MeshColor : MonoBehaviour
{
    public int numberOfSteps = 100;
    public float repeatingTime = 0.1f;
    //public string lowFibPath;
    //public string highFibPath;
    public Color minColor = Color.red, maxColor = Color.blue;
    public Color markingColor = Color.red;
    public GameObject[] colorCubes;
    private int colorIndex = 0;
    private Color[][] originalColorArray;
    private Mesh storedMesh;
    private float[,] voltages;
    private float maxVoltage = float.MinValue;
    private float minVoltage = float.MaxValue;
    private const int DRAWING_CHANEL = 2;


    public Mesh StoredMesh { get { return storedMesh; } set { storedMesh = value; } }
    /// <summary>
    /// Calculates an animation pattern from y_min to y_max of the given mesh. 
    /// Uses the number of steps given by the class variable numberOfSteps. 
    /// Divides the mesh into "numberOfStep" parts (depending on y position) and highlights each segment sequentially,
    /// generating a flow from y_min to y_max.
    /// </summary>
    /// <param name="mesh">Mesh that is animated</param>
    /// <returns>Task object for async handling</returns>
    public async Task CalculateAndStartColorAnimation(Mesh mesh)
    {
        InitColorArray(mesh.vertices.Length);
        await GenerateColorSequence(mesh);
        InvokeRepeating(nameof(ShowColorAnimation), 1, repeatingTime);
        print("Started Color Animation");
    }
    /// <summary>
    /// Initializes the color array as an jacked array with the size [numberOfSteps][numVertices]
    /// With numberOfSteps is an variable from this class.
    /// </summary>
    /// <param name="numVertices">Number of vertices of the mesh which is animated</param>
    private void InitColorArray(int numVertices)
    {
        originalColorArray = new Color[numberOfSteps][];
        for (int i = 0; i < numberOfSteps; i++)
        {
            originalColorArray[i] = new Color[numVertices];
        }
    }

    /// <summary>
    /// Generates an Color sequence which divides the given mesh into numberOfSteps parts according to the y value of each vertex.
    /// Leads to an animation where each segment is highlighted (with a red color) sequentially. 
    /// </summary>
    /// <param name="mesh">The mesh which is animated</param>
    /// <returns>Task object to handle the async call</returns>
    private async Task GenerateColorSequence(Mesh mesh)
    {
        Debug.Log("Update Mesh color started");

        mesh.RecalculateBounds();
        Vector3 minVer = mesh.bounds.min;
        Vector3 maxVer = mesh.bounds.max;

        mesh.colors = new Color[mesh.vertices.Length];
        storedMesh = mesh;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Vector3 offsetCorVec = mesh.vertices[i] - minVer;
            //Index to which segment the point belongs
            int highlightIndex = (int)(offsetCorVec.z / ((maxVer.z - minVer.z) / numberOfSteps));
            for (int j = 0; j < numberOfSteps; j++)
            {
                if (j == highlightIndex)
                {
                    originalColorArray[j][i] = new Color(1, 0, 0);
                }
                else
                {
                    originalColorArray[j][i] = new Color(0, 0, 1);
                }
            }
            //Process in Blocks of 1000
            if (i % 1000 == 0)
            {
                await Task.Yield();
            }

        }
        StatusManager.Instance.SetStatus("Animation calculated");
        Debug.Log("Animation calculated");
    }

    /// <summary>
    /// Function which can be called repeating to create an animation between the steps in the colorArray
    /// Uses the colorIndex Integer for storing the current time index of the animation
    /// </summary>
    public void ShowColorAnimation()
    {
        if (colorIndex >= originalColorArray.Length)
        {
            colorIndex = 0;

        }
        try
        {
            storedMesh.colors = originalColorArray[colorIndex];
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
        //Color.RGBToHSV(currentColorArray[colorIndex][100], out var hue, out var sat, out var val);
        //Debug.Log("Current t: " + hue);


        colorIndex++;
    }

    /// <summary>
    /// Reads in an igb file uses therefore the IGBReader class
    /// </summary>
    /// <param name="path">Path to the igb file</param>
    /// <returns></returns>
    public async Task ReadAnimationFile(string path)
    {
        Debug.Log("Started reading the animation file");

        //init reader
        IGBReader reader = new();
        //Get voltage array
        await reader.Parse(path);
        voltages = reader.AnimationData;
        //Update constants
        numberOfSteps = reader.NumTimestamps;
        maxVoltage = reader.maxValue;
        minVoltage = reader.minValue;
        VoltageLabelHandler.Instance.UpdateMinMidMax(minVoltage, (maxVoltage - minVoltage) / 2 + minVoltage, maxVoltage);

        //Update and log status
        StatusManager.Instance.SetStatus("Finished reading the animation file");
        Debug.Log("Finished reading the animation file");
        Debug.Log("Min Voltage: " + minVoltage);
        Debug.Log("Max Voltage: " + maxVoltage);
    }



    /// <summary>
    /// Converts th read in voltages from the igb file into an (color) animation for the given mesh
    /// </summary>
    /// <param name="mesh">The mesh on which the animation is applied on</param>
    /// <returns></returns>
    public async Task CreateAnimationForIGBFile(Mesh mesh)
    {

        //Update mesh
        if (storedMesh != mesh)
        {
            storedMesh = mesh;
            ResetUVForDrawing();
        }
        Debug.Log("Started interpolating colors");
        StatusManager.Instance.SetStatus("Started calculating color values");
        InitColorArray(voltages.GetLength(1));

        //GetLength(1) returns the number of vertices
        for (int i = 0; i < voltages.GetLength(1); i++)
        {
            for (int j = 0; j < numberOfSteps; j++)
            {
                //generate new color with the calculateColor method
                originalColorArray[j][i] = CalculateColor(voltages[j, i]);
            }

            //Breaks to free the main thread while computing and log the current state
            if (i % 1000 == 0)
            {
                StatusManager.Instance.SetStatus(i.ToString("#,##0") + " from " + mesh.vertices.Length.ToString("#,##0") + " colors calculated");
                await Task.Yield();

            }
        }



        //Sets Cubes to show min mid and max values
        ColorReferenceCubes();
        Debug.Log("Finished interpolating colors");
        StatusManager.Instance.SetStatus("Finished calculating color values");
        Debug.Log("Copy color array");
        //Stop all animation to avoid bad array access while updating 
        CancelInvoke();
        Debug.Log("Finished Copying color array");
        await generateFibrosis();
        //Starts the animation of the mesh
        InvokeRepeating(nameof(ShowColorAnimation), 1, repeatingTime);
    }

    public void ColorByElement(ElementParser parser)
    {
        List<int> elements = parser.ReadInElementTags;
        for (int i = 0; i < elements.Count; i++)
        {

            PaintTriangle(i, Color.Lerp(Color.blue, Color.red, (float)elements[i] / 10));
        }


    }

    /// <summary>
    /// Starts the process of generating severe and mild fibrosis 
    /// Updates uv coordinates to display fibrotic tissue in the graphics shader
    /// </summary>
    /// <returns></returns>
    public async Task generateFibrosis()
    {
        FibrosisParser fibParseHigh = new FibrosisElementParser();
        FibrosisParser fibParseMid = new FibrosisElementParser();
        string highFibPath = "";
        string lowFibPath = "";
        foreach (string file in Directory.GetFiles(PlayerPrefs.GetString(Constants.dataDirectory), "*.regele"))
        {
            if (file.Contains("not_conductive"))
            {
                highFibPath = file;
            }
            else if (file.Contains("slow_conductive"))
            {
                lowFibPath = file;
            }
        }
        await fibParseHigh.Parse(highFibPath);
        await fibParseMid.Parse(lowFibPath);
        List<int> midFibrosis = fibParseMid.FibrosisElements;
        List<int> highFibrosis = fibParseHigh.FibrosisElements;

        FibrosisCalculator fibCalc = new FibrosisCalculator(storedMesh, midFibrosis, highFibrosis);
        fibCalc.generateUVForFibrosis();
        storedMesh.SetUVs(1, fibCalc.FibrosisPointWise);
    }



    /// <summary>
    /// Calculates a color corresponding to the input value.
    /// This value should be between 0 and 1
    /// </summary>
    /// <param name="value">Interpolating value, should be between 0 and 1</param>
    /// <returns>Color corresponding to the current coloring method</returns>
    private Color CalculateColor(float value)
    {
        float t = Mathf.InverseLerp(minVoltage, maxVoltage, value);

        //Color oldColor = colTestArr[(int)t];
        // Color newColor = colTestArr[(int)(t + 1f)];

        //Interpolate the hue value for the animation (shifted so 0 is blue and 1 is red)
        return Color.HSVToRGB(Mathf.Clamp(0.7f - t, 0, 1), 1, 1);
    }

    /// <summary>
    /// Takes the 3 reference cubes to show the color spectrum
    /// </summary>
    private void ColorReferenceCubes()
    {
        colorCubes[0].GetComponent<MeshRenderer>().material.color = CalculateColor(minVoltage);
        colorCubes[1].GetComponent<MeshRenderer>().material.color = CalculateColor((maxVoltage - minVoltage) / 2 + minVoltage);
        colorCubes[2].GetComponent<MeshRenderer>().material.color = CalculateColor(maxVoltage);
    }




    /// <summary>
    /// Stops and starts the animation to reset it
    /// </summary>
    private void ResetAnimation()
    {
        CancelInvoke();
        InvokeRepeating(nameof(ShowColorAnimation), 1, repeatingTime);
    }


    /// <summary>
    /// Paints the vertices of a triangle into a specific color 
    /// </summary>
    /// <param name="triangleIndex">Index of the triangle which should be colored</param>
    public void PaintTriangle(int triangleIndex, Color color)
    {
        PaintVertices(GetEdgesFromTriangle(triangleIndex), color);
    }

    public void ResetUVForDrawing()
    {
        List<Vector2> uvs = new List<Vector2>();
        storedMesh.GetUVs(DRAWING_CHANEL, uvs);
        uvs = HelperMethods.InitZeroV2Array(storedMesh.vertices.Length);
        storedMesh.SetUVs(DRAWING_CHANEL, uvs);
    }





    /// <summary>
    /// Paints vertices with a given color
    /// </summary>
    /// <param name="vertices"></param>
    /// <param name="color"></param>
    public void PaintVertices(int[] vertices, Color color)
    {
        List<Vector2> uvEntries = new List<Vector2>();
        storedMesh.GetUVs(DRAWING_CHANEL, uvEntries);
        if (uvEntries.Count < 1)
        {
            uvEntries = InitListWithZeros(storedMesh.vertices.Length);
            storedMesh.SetUVs(DRAWING_CHANEL, uvEntries);
        }
        for (int i = 0; i < numberOfSteps; i++)
        {
            foreach (int vertex in vertices)
            {
                uvEntries[vertex] = new Vector2(1, 0);
                //currentColorArray[i][vertex] = color;
            }
        }
        storedMesh.SetUVs(DRAWING_CHANEL, uvEntries);
    }

    private List<Vector2> InitListWithZeros(int numItems)
    {
        List<Vector2> list = new List<Vector2>(numItems);
        for (int i = 0; i < numItems; i++)
        {
            list.Add(Vector2.zero);
        }
        return list;
    }

    /// <summary>
    /// Resets the marking of one triangle in the array
    /// </summary>
    /// <param name="index">The index of the vertex which should be reseted</param>
    public void ResetOneTriangle(int triangleIndex)
    {
        int[] indices = GetEdgesFromTriangle(triangleIndex);
        Vector2[] entries = new Vector2[indices.Length];
        for (int i = 0; i < entries.Length; i++)
        {
            entries[i] = new Vector2(0, 0);
        }
        SetUvOfMesh(entries, indices, 2);
    }

    public List<float> getMarkings()
    {
        //
        List<Vector2> markingsVectorArray = new List<Vector2>();
        List<float> markedList = new List<float>();

        storedMesh.GetUVs(DRAWING_CHANEL, markingsVectorArray);
        foreach (Vector2 entry in markingsVectorArray)
        {
            //Just stored in first entry of uv coordinates
            markedList.Add(entry.x);
        }
        return markedList;
    }

    public void setMarkings(List<float> markings)
    {
        List<Vector2> markingList = new List<Vector2>();
        foreach (float mark in markings)
        {
            markingList.Add(new Vector2(mark, 0));
        }
        storedMesh.SetUVs(DRAWING_CHANEL, markingList);
    }

    private void SetUvOfMesh(Vector2[] entries, int[] indices, int channel)
    {
        if (entries.Length != indices.Length)
        {
            StatusManager.Instance.SetStatus("Number of entries " + entries.Length + " to reset does not match th number of indices " + indices.Length);
            return;
        }
        List<Vector2> uvCoords = new List<Vector2>();
        storedMesh.GetUVs(channel, uvCoords);
        for (int i = 0; i < indices.Length; i++)
        {
            uvCoords[indices[i]] = entries[i];
        }

        storedMesh.SetUVs(channel, uvCoords);
    }

    private int[] GetEdgesFromTriangle(int triangleIndex)
    {
        int vertex0 = storedMesh.triangles[triangleIndex * 3];
        int vertex1 = storedMesh.triangles[triangleIndex * 3 + 1];
        int vertex2 = storedMesh.triangles[triangleIndex * 3 + 2];

        return new int[] { vertex0, vertex1, vertex2 };
    }



    /// <summary>
    /// Colors the mesh relative to the position of each vertex, meaning that 
    /// </summary>
    /// <param name="mesh"></param>
    /// <returns></returns>
    private async Task UpdateMeshColor(Mesh mesh)
    {
        print("Update Mesh color started");
        Vector3 minVer = mesh.bounds.min;
        Vector3 maxVer = mesh.bounds.max;
        Color[] colors = new Color[mesh.vertices.Length];
        Vector3 vectorOffset = (maxVer - minVer);
        Vector3 scaleVert = new(1 / vectorOffset.x, 1 / vectorOffset.y, 1 / vectorOffset.z);
        for (int i = 0; i < colors.Length; i++)
        {
            Vector3 offsetCorVec = mesh.vertices[i] - minVer;
            float r, g, b;
            r = offsetCorVec.x * scaleVert.x;
            g = offsetCorVec.y * scaleVert.y;
            b = offsetCorVec.z * scaleVert.z;
            //colors[i] = new Color(255,0,0);
            colors[i] = new Color(r, g, b);
            //Process in Blocks of 1000
            if (i % 1000 == 0)
            {
                //print("ping: " + i);
                await Task.Yield();
            }

        }
        mesh.colors = colors;
        print("New Mesh Color added");
    }
}
