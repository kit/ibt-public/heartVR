using UnityEngine;

/// <summary>
/// Manages the update process of new .igb and geometry files from the front end
/// </summary>
public class UpdateFoldersAndFiles : MonoBehaviour
{
    public MeshGenerator meshGen;
    private string lastAnimationPath;
    private string lastMeshPath;
    private bool generated = false; //Determines if the current configuration was generated
    // Start is called before the first frame update
    void Start()
    {
        UpdateLastPaths();
    }
    public async void CheckForUpdate()
    {
        string currentMeshPath = PlayerPrefs.GetString(Constants.dataDirectory, string.Empty);
        string currentAnimationPath = PlayerPrefs.GetString(Constants.animationFilePath, string.Empty);
        if (!generated || (currentMeshPath != lastMeshPath && currentAnimationPath != lastAnimationPath))
        {
            generated = true;
            //StatusManager.Instance.SetStatus("Start generating mesh and animation");
            await meshGen.UpdateMeshAndAnimation();
        }
        else if (currentMeshPath != lastMeshPath)
        {
            generated = true;
            Debug.LogWarning("Mesh but not Animation Updated");
            await meshGen.UpdateMeshAndAnimation();
        }
        else if (currentAnimationPath != lastAnimationPath)
        {
            generated = true;
            StatusManager.Instance.SetStatus("Start generating only new animation");
            await meshGen.UpdateAnimationData();
        }
        Start();
    }

    void UpdateLastPaths()
    {
        lastMeshPath = PlayerPrefs.GetString(Constants.dataDirectory, string.Empty);
        lastAnimationPath = PlayerPrefs.GetString(Constants.animationFilePath, string.Empty);
    }
}
