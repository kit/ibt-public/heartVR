using System;

public static class Constants
{
    public const string dataDirectory = "DATA_DIR";
    public const string animationFilePath = "ANIM_PATH";
    public const string drawingFilePath = "DRAW_PATH";

    public static string GetPathKey(string variable)
    {
        if (variable == "IGB_DIR")
        {
            return dataDirectory;
        }
        else if (variable == "ANIM_PATH")
        {
            return animationFilePath;
        }
        throw new ArgumentException("PlayerPref key" + variable + " not found");
    }
}
