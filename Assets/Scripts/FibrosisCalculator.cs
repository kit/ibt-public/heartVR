﻿using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace Assets.Scripts
{
    internal class FibrosisCalculator
    {
        private readonly Mesh _mesh;
        private List<int> MidFibrosis;
        private List<int> HighFibrosis;
        private List<Vector2> fibrosisPointwise;
        private Dictionary<int, int> NumberOfTrianglPerVertex;

        public List<Vector2> FibrosisPointWise { get { return fibrosisPointwise; } }
        public FibrosisCalculator(Mesh mesh, List<int> midFibrosis, List<int> highFibrosis)
        {
            _mesh = mesh;
            MidFibrosis = midFibrosis;
            HighFibrosis = highFibrosis;
            NumberOfTrianglPerVertex = new Dictionary<int, int>();
            fibrosisPointwise = HelperMethods.InitZeroV2Array(mesh.vertices.Length);
        }

        public void generateUVForFibrosis()
        {
            //CountTrianglesPerVertex();
            StatusManager.Instance.SetStatus("Started calculation for middle Fibrosis");
            AddFibrosis(0.5f, MidFibrosis);
            StatusManager.Instance.SetStatus("Started calculation for high Fibrosis");
            AddFibrosis(1, HighFibrosis);
            //WeightFibrosis();
            StatusManager.Instance.SetStatus("Finished fibrosis calculations");
        }


        /// <summary>
        /// Calculates ho many triangles are attached to an vertex
        /// </summary>
        private void CountTrianglesPerVertex()
        {
            foreach (int vertex in _mesh.triangles)
            {
                if (NumberOfTrianglPerVertex.ContainsKey(vertex))
                {
                    NumberOfTrianglPerVertex[vertex]++;
                }
                else
                {
                    NumberOfTrianglPerVertex[vertex] = 1;
                }
            }

            //Half of the number is the real one, because triangles are drawn twice
            foreach (var key in NumberOfTrianglPerVertex.Keys.ToList())
            {
                NumberOfTrianglPerVertex[key] /= 2;
            }
        }


        private void AddFibrosis(float severity, List<int> triangleIndices)
        {
            //int numAdddIndices = 0;


            int factorDuplicate = 2;
            NativeArray<Vector3> vertexIndices = new NativeArray<Vector3>(triangleIndices.Count, Allocator.Persistent);
            NativeArray<int> triangleIndicesNative = new NativeArray<int>(triangleIndices.ToArray(), Allocator.Persistent);
            NativeArray<int> meshTriangles = new NativeArray<int>(_mesh.triangles, Allocator.Persistent);

            var Job = new CalculateVerticesForTrianglesJob()
            {
                factor = factorDuplicate * 3,
                meshTriangles = meshTriangles,
                triangleIndices = triangleIndicesNative,
                vertexIndices = vertexIndices
            };
            JobHandle jobHandle = Job.Schedule(triangleIndicesNative.Length, 64);
            jobHandle.Complete();
            foreach (Vector3 vertexIndex in vertexIndices)
            {
                fibrosisPointwise[(int)vertexIndex.x] = new Vector2(severity, 0);
                fibrosisPointwise[(int)vertexIndex.y] = new Vector2(severity, 0);
                fibrosisPointwise[(int)vertexIndex.z] = new Vector2(severity, 0);
            }


            vertexIndices.Dispose();
            triangleIndicesNative.Dispose();
            meshTriangles.Dispose();
        }


        /// <summary>
        /// Looks for the corresponding vertices for one element
        /// </summary>
        struct CalculateVerticesForTrianglesJob : IJobParallelFor
        {
            [Unity.Collections.ReadOnly]
            public NativeArray<int> triangleIndices;

            [Unity.Collections.ReadOnly]
            public NativeArray<int> meshTriangles;
            public int factor;

            public NativeArray<Vector3> vertexIndices;
            public void Execute(int index)
            {
                int startingIndex = (triangleIndices[index]) * factor;
                if (startingIndex + 2 >= meshTriangles.Length)
                {
                    return;
                }
                //find neighbor vertices
                int vertex0 = meshTriangles[startingIndex];
                int vertex1 = meshTriangles[startingIndex + 1];
                int vertex2 = meshTriangles[startingIndex + 2];


                //Ad fibrosis modifier to vertex
                vertexIndices[index] = new Vector3(vertex0, vertex1, vertex2);
            }
        }


        /// <summary>
        /// Creates Mean of all added Fibrosis to interpolate the triangles to points
        /// </summary>
        private void WeightFibrosis()
        {
            for (int vertexIndex = 0; vertexIndex < fibrosisPointwise.Count; vertexIndex++)
            {
                if (fibrosisPointwise[vertexIndex].x != 0)
                {
                    fibrosisPointwise[vertexIndex] /= NumberOfTrianglPerVertex[vertexIndex];
                }
            }
        }
    }
}
