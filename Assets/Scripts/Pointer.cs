using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Creates and manages the pointer to point to objects
/// </summary>
public class Pointer : MonoBehaviour
{
    public float m_default_length = 5.0f; //Maximum length of the ray
    public GameObject m_Dot; //Dot at the end of the ray
    public VrInputModule m_Modul; // Input module which handles the interactions

    private LineRenderer m_LineRenderer = null; //Line which is drawn between the attached object and the end point

    // Start is called before the first frame update
    void Start()
    {
        m_LineRenderer = GetComponent<LineRenderer>();
    }

    private void UpdateLine()
    {
        //Select length default/length
        PointerEventData data = m_Modul.GetData();

        //Set new distance if hit
        float targetLength = data.pointerCurrentRaycast.distance == 0 ? m_default_length : data.pointerCurrentRaycast.distance;

        //Ray-cast
        RaycastHit hit = CreateRaycast();

        //Default pointer end
        Vector3 endPostion = transform.position + (transform.forward * targetLength);

        //Update end position if it hits an object
        if (hit.collider != null)
        {
            endPostion = hit.point;
        }

        //Position Dot update
        m_Dot.transform.position = endPostion;

        //Set line Renderer
        m_LineRenderer.SetPosition(0, transform.position);
        m_LineRenderer.SetPosition(1, endPostion);
    }

    /// <summary>
    /// Generates a new ray-cast from the attached object
    /// </summary>
    /// <returns>A newly generated Ray-cast</returns>
    private RaycastHit CreateRaycast()
    {
        Ray ray = new(transform.position, transform.forward);
        Physics.Raycast(ray, out RaycastHit hit, m_default_length);

        return hit;
    }

    // Updates the pointer Line, called once per frame
    void Update()
    {
        UpdateLine();
    }
}
