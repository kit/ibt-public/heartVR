using System;
using UnityEngine;

public class TestReadInBinary : MonoBehaviour
{
    public IGBReader reader;
    public MeshColor meshcolor;
    public string filePath;
    // Start is called before the first frame update
    async void Start()
    {
        DateTime now = DateTime.Now;
        await reader.Parse(filePath + ".igb");
        Debug.Log("Binary has taken" + DateTime.Now.Subtract(now).TotalSeconds + " s");
        now = DateTime.Now;
        await meshcolor.ReadAnimationFile(filePath + ".txt");
        Debug.Log("Text file has taken" + DateTime.Now.Subtract(now).TotalSeconds + " s");
    }
}
